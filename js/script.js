$(document).ready(function(){
    var hamburgerButton = $("#hamburger");
    var hamburgerButtonOpen = $("#hamburger.open");
    var mainMenuMobile = $("#menuMobile");

    //OPEN MENU MOBILE
    hamburgerButton.on("click",function(){
        $(this).toggleClass('open');
        mainMenuMobile.toggleClass('showMenuMobile')
    });

    //Carousel
    $('#myCarousel').carousel({
        interval: 10000
    });


    //HIDE and SHOW MENU

    $(window).scroll(function() {
        if($( window ).width()>=768) {
            var scroll = $(window).scrollTop();
            var section_height = $("#mainSection").height();
            var button_height = section_height - 48;

            var section_subsite_height = $("#mainSectionSubsite").height();
            var menu_subsite_height =$(".mainScrollSubsite").height();


            if ( (scroll >= button_height) ) {
                $("header#mainScroll").addClass("menuShow");
                $("header#menu").addClass("menuHide");
            }
            else if(scroll <= button_height && scroll >= button_height){
                $("header#menu").removeClass("menuHide");
            }

            else{
                $("header#mainScroll").removeClass("menuShow");
                $("header#menu").removeClass("menuHide");
            }


            if ( (scroll >= section_subsite_height) ) {
                $("header#mainScroll").addClass("mainScrollFixed");
            }
            else if(scroll <= section_subsite_height && scroll >= section_subsite_height){
                $("header#mainScroll").removeClass("mainScrollFixed");
            }
            else{
                $("header#mainScroll").removeClass("mainScrollFixed");
            }
        }
    });

    //Specialization
    var idButton = [];

    $(".showBox button").each(function(){
       idButton.push($(this).attr("id"));
    });

    var t = 0;
    $.each(idButton,function(){
        var ButtonMore = "#" + idButton[t] + "More";
        $("#" + idButton[t]).on("click",function(){
            $(this).toggleClass("specializationButtonBlue");
            $(ButtonMore).slideToggle();
        });
        t++;
    });

});