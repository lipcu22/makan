function initMap() {
    var mapDiv = document.getElementById('map');
    var myPosition = {lat: 50.070955, lng: 19.919253};

    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(mapDiv, {
        scrollwheel: false,
        center: myPosition,
        zoom: 16,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": "-2"
                    },
                    {
                        "lightness": "-8"
                    },
                    {
                        "gamma": "1.26"
                    },
                    {
                        "weight": "1.22"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "lightness": "-15"
                    },
                    {
                        "gamma": "1.57"
                    },
                    {
                        "weight": "0.01"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#979797"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#c9e4ee"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]
    });

    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: myPosition,
        icon:'images/map-pin.png'
    });

}
