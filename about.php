<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>O kancelarii</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,600,700' rel='stylesheet'
          type='text/css'>

    <!-- Bootstrap -->
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>

    <script src="js/script.js"></script>

    <!-- MAP -->
    <script type="text/javascript" src="js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
            async defer></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top">
<section id="mainSectionSubsite" class="mainSection mainSectionSubsite " style="background-image: url('images/mainsmall.jpg')">
    <div class="container-fluid nopadding minheight">
        <div id="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <div class="imagePattern"></div>
        <div class="blackPattern"></div>

        <!-- Menu mobile -->
        <header id="menuMobile" class="menuMobile">
            <ul class="navMenuMobile">
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">O KANCELARII</a></li>
                <li><a href="team.php">ZESPÓŁ</a></li>
                <li><a href="specialization.php">SPECJALIZACJE</a></li>
                <li><a href="contact.php">KONTAKT</a></li>
            </ul>
        </header>

    </div>
</section>

<!-- Menu scroll -->
<header id="mainScroll" class="mainScrollSubsite">
    <div class="logoMainScroll">
        <a href="index.php"><img src="images/logoMainScroll.svg" alt="logo"></a>
    </div>
    <div class="menuHolder">
        <ul class="navMenuScroll">
            <li><a href="index.php">HOME</a></li>
            <li><a href="about.php">O KANCELARII</a></li>
            <li><a href="team.php">ZESPÓŁ</a></li>
            <li><a href="specialization.php">SPECJALIZACJE</a></li>
            <li><a href="contact.php">KONTAKT</a></li>
        </ul>
    </div>
    <div class="socialHolder">
        <ul class="social">
            <li>
                <div class="mailSocial">
                    <button class="buttonMail"  id="dropdownmail" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="images/iconmail.png" alt="">
                    </button>
                    <ul class="dropdown-menu mailDropDown" aria-labelledby="dropdownmail">
                        <li><a href="mailto:kancelaria@kancelaria-kpml.pl">kancelaria@kancelaria-kpml.pl</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="phoneSocial">
                    <button class="buttonPhone"  id="dropdownphone" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="images/iconphone.png" alt="">
                    </button>
                    <ul class="dropdown-menu phoneDropDown" aria-labelledby="dropdownphone">
                        <li><a href="tel:+48 533 920 115">+48 533 920 115</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>

</header>

<section id="about" class="about">
    <div class="container-fluid nopadding ">
        <div class="sectionBox">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>O KANCELARII</h2>
            </div>
            <div class="breakLineCenter"></div>
            <div class="aboutBox">
                <ul>
                    <li>Kancelaria Prawna Joanna Makan & Joanna Kwinta Radcowie Prawni s.c. została zawiązana przez radców prawnych Joannę Makan i Joannę Kwintę w 2011 roku, jako spółka cywilna.</li>
                    <li><span>Naszym celem jest zapewnienie profesjonalnego doradztwa prawnego zarówno na rzecz podmiotów gospodarczych jak i osób fizycznych.</span></li>
                    <li>Prawnicy Kancelarii posiadają specjalistyczną wiedzę z licznych dziedzin prawa, co umożliwia pełną i kompleksową obsługę Klienta. Kancelaria zapewnia kompleksową obsługę prawną poprzez współpracę z kancelarią notarialną, adwokacką, jak również kancelarią doradcy podatkowego.</li>
                    <li>Pomoc prawną świadczymy w języku polskim oraz w języku angielskim.</li>
                </ul>
            </div>
        </div>

    </div>
</section>

<section id="aboutDescription" class="aboutDescription" style="background-image: url('images/aboutdescription.jpg')">
    <div class="container-fluid nopadding">
        <div class="imagePattern"></div>
        <div class="blackPattern2"></div>

        <div class="sectionBox">
            <div class="mainDescription sectionDescription colorTextWhite">
                <h2>CO NAS WYRÓŻNIA</h2>
            </div>
            <div class="breakLineCenter"></div>
            <div class="aboutDescription colorTextWhite">
                <ul>
                    <li>Lorem ipsum dolor sit amet, no sumo quaeque efficiendi eam, agam eligendi quo eu, nam novum utinam sensibus ea. Natum volumus probatus no usu, altera impetus iudicabit qui id, cu sit virtute maiestatis. </li>
                    <li>Sit rebum complectitur no, no indoctum salutatus omittantur qui. Primis petentium mea in. Inani doctus saperet id eos, an labitur recteque adipiscing eam, semper senserit eum in.</li>
                    <li>Natum volumus probatus no usu, altera impetus iudicabit qui id, cu sit virtute maiestatis.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="aboutIconSection" class="aboutIconSection">
    <div class="IconContainer container-fluid nopadding">
        <div class="row">
            <div class="col-xs-12 col-sm-4 aboutIconBox">
                <div class="aboutIcon">
                    <img src="images/employee2.png" class="img-responsive" alt="employee">
                </div>
                <div class="aboutIconDescription">
                    <p>RZETELNE I PROFESJONALNE USŁUGI PRAWNICZE</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 aboutIconBox">
                <div class="aboutIcon">
                    <img src="images/certificate.png" class="img-responsive" alt="certificate">
                </div>
                <div class="aboutIconDescription">
                    <p>PEWNOŚĆ, ŻE PAŃSTWA SPRAWY SPOCZYWAJĄ W DOBRYCH RĘKACH</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 aboutIconBox">
                <div class="aboutIcon">
                    <img src="images/worker.png" class="img-responsive" alt="worker">
                </div>
                <div class="aboutIconDescription">
                    <p>POCZUCIE BEZPIECZEŃSTWA I SATYSFAKCJI</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="map_page" class="map_page">
    <div id="map">
    </div>
</section>

<section id="contactForm" class="contactForm">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter sectionCenter">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>NAPISZ DO NAS</h2>
            </div>
            <div class="breakLineCenter"></div>
        </div>
        <div class="form row">
            <form>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="name" class="form-control contact-form" id="inputName" placeholder="Wpisz Imię i Nazwisko">
                    <input type="text" name="company name" class="form-control contact-form" id="inputCompany" placeholder="Wpisz nazwę firmy">
                    <input type="tel" name="phone" class="form-control contact-form" id="inputPhone" placeholder="Wpisz numer telefonu">
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="topic" class="form-control contact-form" id="inputSubject" placeholder="Wpisz temat">
                    <textarea class="form-control contact-area" id="inputMessage" placeholder="Wpisz treść wiadomości..."></textarea>
                </div>
            </form>
        </div>
        <a href="#" class="buttonMore buttonMore-1 buttonMore-2">wyślij</a>
        <div class="iconContact ">
            <ul>
                <li><a href="tel:12 349 06 84"><img src="images/iconphone.png" alt="phone">12 349 06 84</a></li>
                <li><a href="mailto:kancelaria@kancelaria-kpml.pl"><img src="images/iconmail.png" alt="mail">kancelaria@kancelaria-kpml.pl</a></li>
            </ul>
        </div>
    </div>
</section>

<section id="footerLogo" class="footerLogo">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter">
            <div class="logoFooter">
                <a href="index.php"><img src="images/logomakan.svg" alt="logo"></a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright colorTextWhite"><p>JOANNA MAKAN 2016 | Wykonanie: <a class="colorTextRed" href="#">BeSite.pl</a></p></div>
</footer>

</body>

</html>