<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Specjalizacje</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,600,700' rel='stylesheet'
          type='text/css'>

    <!-- Bootstrap -->
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>

    <script src="js/script.js"></script>

    <!-- MAP -->
    <script type="text/javascript" src="js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
            async defer></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top">
<section id="mainSectionSubsite" class="mainSection mainSectionSubsite " style="background-image: url('images/mainsmall.jpg')">
    <div class="container-fluid nopadding minheight">
        <div id="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <div class="imagePattern"></div>
        <div class="blackPattern"></div>

        <!-- Menu mobile -->
        <header id="menuMobile" class="menuMobile">
            <ul class="navMenuMobile">
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">O KANCELARII</a></li>
                <li><a href="team.php">ZESPÓŁ</a></li>
                <li><a href="specialization.php">SPECJALIZACJE</a></li>
                <li><a href="contact.php">KONTAKT</a></li>
            </ul>
        </header>

    </div>
</section>

<!-- Menu scroll -->
<header id="mainScroll" class="mainScrollSubsite">
    <div class="logoMainScroll">
        <a href="index.php"><img src="images/logoMainScroll.svg" alt="logo"></a>
    </div>
    <div class="menuHolder">
        <ul class="navMenuScroll">
            <li><a href="index.php">HOME</a></li>
            <li><a href="about.php">O KANCELARII</a></li>
            <li><a href="team.php">ZESPÓŁ</a></li>
            <li><a href="specialization.php">SPECJALIZACJE</a></li>
            <li><a href="contact.php">KONTAKT</a></li>
        </ul>
    </div>
    <div class="socialHolder">
        <ul class="social">
            <li>
                <div class="mailSocial">
                    <button class="buttonMail"  id="dropdownmail" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="images/iconmail.png" alt="">
                    </button>
                    <ul class="dropdown-menu mailDropDown" aria-labelledby="dropdownmail">
                        <li><a href="mailto:kancelaria@kancelaria-kpml.pl">kancelaria@kancelaria-kpml.pl</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="phoneSocial">
                    <button class="buttonPhone"  id="dropdownphone" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="images/iconphone.png" alt="">
                    </button>
                    <ul class="dropdown-menu phoneDropDown" aria-labelledby="dropdownphone">
                        <li><a href="tel:+48 533 920 115">+48 533 920 115</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>

</header>

<section id="specializationSection" class="specializationSection">
    <div class="container">
        <div class="mainDescription sectionDescription colorTextGrey">
            <h2>SPECJALIZACJE</h2>
        </div>
        <div class="breakLineCenter"></div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton1" class="specializationButton" data-target="#show1">Prawo cywilne</button>
                <div id="showButton1More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton2" class="specializationButton" data-target="#show1">Prawo rodzinne</button>
                <div id="showButton2More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton3" class="specializationButton" data-target="#show1">Prawo pracy</button>
                <div id="showButton3More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton4" class="specializationButton" data-target="#show1">Prawo zamówień publicznych</button>
                <div id="showButton4More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton5" class="specializationButton" data-target="#show1">Windykacja należności</button>
                <div id="showButton5More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton6" class="specializationButton" data-target="#show1">Prawo nieruchomości</button>
                <div id="showButton6More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 showBox">
                <button type="button" id="showButton7" class="specializationButton" data-target="#show1">Prawo rodzinne</button>
                <div id="showButton7More" class="showMore">
                    <ul>
                        <li>Zapewniamy kompleksową pomoc prawną przy sporządzaniu i opiniowaniu umów cywilnoprawnych, przygotowywaniu pism procesowych oraz reprezentacji Klientów w sprawach sądowych z zakresu prawa cywilnego, prawa spadkowego,prawa zobowiązań, prawa upadłościowego i egzekucyjnego, a także prawa ubezpieczeniowego.</li>
                        <li>Oferujemy Państwu swoje doświadczenie, poparte bogatą praktyką z zakresu doradztwa i reprezentowania Klienta, w tej jakże obszernej tematyce, jaką jest prawo cywilne.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="map_page" class="map_page">
    <div id="map">
    </div>
</section>

<section id="contactForm" class="contactForm">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter sectionCenter">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>NAPISZ DO NAS</h2>
            </div>
            <div class="breakLineCenter"></div>
        </div>
        <div class="form row">
            <form>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="name" class="form-control contact-form" id="inputName" placeholder="Wpisz Imię i Nazwisko">
                    <input type="text" name="company name" class="form-control contact-form" id="inputCompany" placeholder="Wpisz nazwę firmy">
                    <input type="tel" name="phone" class="form-control contact-form" id="inputPhone" placeholder="Wpisz numer telefonu">
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="topic" class="form-control contact-form" id="inputSubject" placeholder="Wpisz temat">
                    <textarea class="form-control contact-area" id="inputMessage" placeholder="Wpisz treść wiadomości..."></textarea>
                </div>
            </form>
        </div>
        <a href="#" class="buttonMore buttonMore-1 buttonMore-2">wyślij</a>
        <div class="iconContact ">
            <ul>
                <li><a href="tel:12 349 06 84"><img src="images/iconphone.png" alt="phone">12 349 06 84</a></li>
                <li><a href="mailto:kancelaria@kancelaria-kpml.pl"><img src="images/iconmail.png" alt="mail">kancelaria@kancelaria-kpml.pl</a></li>
            </ul>
        </div>
    </div>
</section>

<section id="footerLogo" class="footerLogo">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter">
            <div class="logoFooter">
                <a href="index.php"><img src="images/logomakan.svg" alt="logo"></a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright colorTextWhite"><p>JOANNA MAKAN 2016 | Wykonanie: <a class="colorTextRed" href="#">BeSite.pl</a></p></div>
</footer>

</body>

</html>