<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Joanna Makan</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,600,700' rel='stylesheet'
          type='text/css'>

    <!-- Bootstrap -->
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>

    <script src="js/script.js"></script>

    <!-- MAP -->
    <script type="text/javascript" src="js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
            async defer></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top">
<section id="mainSection" class="mainSection" style="background-image: url('images/mainbg.jpg')">
    <div class="container-fluid nopadding minheight">
        <div id="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <div class="imagePattern"></div>
        <div class="blackPattern"></div>
        <div class="mainSectionCenter">
            <div class="logo">
                <img src="images/logomakan.svg" alt="logo">
            </div>
            <div class="mainDescription colorTextWhite">
                <h2>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid autem blanditiis cum illo libero
                    nostrum pariatur perferendis quia reiciendis vel"</h2>
            </div>
        </div>
        <div class="bottomArrow">
            <a class="page-scroll" href="#offer">
                <img src="images/bottomarrow.png" alt="arrowbottom">
            </a>
        </div>

        <!-- Main menu -->
        <header id="menu">
            <ul class="navMenu">
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">O KANCELARII</a></li>
                <li><a href="team.php">ZESPÓŁ</a></li>
                <li><a href="specialization.php">SPECJALIZACJE</a></li>
                <li><a href="contact.php">KONTAKT</a></li>
            </ul>
        </header>

        <!-- Menu mobile -->
        <header id="menuMobile" class="menuMobile">
            <ul class="navMenuMobile">
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">O KANCELARII</a></li>
                <li><a href="team.php">ZESPÓŁ</a></li>
                <li><a href="specialization.php">SPECJALIZACJE</a></li>
                <li><a href="contact.php">KONTAKT</a></li>
            </ul>
        </header>

        <!-- Menu scroll -->
        <header id="mainScroll">
            <div class="logoMainScroll">
                <a  href="index.php"><img src="images/logoMainScroll.svg" alt="logo"></a>
            </div>
            <div class="menuHolder">
                <ul class="navMenuScroll">
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="about.php">O KANCELARII</a></li>
                    <li><a href="team.php">ZESPÓŁ</a></li>
                    <li><a href="specialization.php">SPECJALIZACJE</a></li>
                    <li><a href="contact.php">KONTAKT</a></li>
                </ul>
            </div>
            <div class="socialHolder">
                <ul class="social">
                    <li>
                        <div class="mailSocial">
                            <button class="buttonMail" id="dropdownmail" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="true">
                                <img src="images/iconmail.png" alt="">
                            </button>
                            <ul class="dropdown-menu mailDropDown" aria-labelledby="dropdownmail">
                                <li><a href="mailto:kancelaria@kancelaria-kpml.pl">kancelaria@kancelaria-kpml.pl</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="phoneSocial">
                            <button class="buttonPhone" id="dropdownphone" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="true">
                                <img src="images/iconphone.png" alt="">
                            </button>
                            <ul class="dropdown-menu phoneDropDown" aria-labelledby="dropdownphone">
                                <li><a href="tel:+48 533 920 115">+48 533 920 115</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

        </header>
    </div>
</section>

<section id="offer" class="offer">
    <div class="container-fluid nopadding">
        <div class="sectionBox">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>CO OFERUJEMY?</h2>
            </div>
            <div class="breakLineCenter"></div>


            <div class="offerCarousel">
                <div class="carousel slide" id="myCarousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/employee.png" class="img-responsive" alt="employee">
                                    </div>
                                    <div class="offerDescription">
                                        <p>BIEŻĄCE DORADZTWO PRAWNE</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/interrogation.png" class="img-responsive" alt="interrogation">
                                    </div>
                                    <div class="offerDescription">
                                        <p>KOMPLEKSOWA OBSŁUGA PODMITÓW GOSPODARCZYCH</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/handshake.png" class="img-responsive" alt="handshake">
                                    </div>
                                    <div class="offerDescription">
                                        <p>PROWADZENIE NEGOCJACJI W IMIENIU KLIENTA</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/tests.png" class="img-responsive" alt="test">
                                    </div>
                                    <div class="offerDescription">
                                        <p>SPORZĄDZANIE UMÓW I OPINI</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/working-out.png" class="img-responsive" alt="working">
                                    </div>
                                    <div class="offerDescription">
                                        <p>ZASTĘPSTWO PROCESOWE</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/employee.png" class="img-responsive" alt="employee">
                                    </div>
                                    <div class="offerDescription">
                                        <p>BIEŻĄCE DORADZTWO PRAWNE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/interrogation.png" class="img-responsive" alt="interrogation">
                                    </div>
                                    <div class="offerDescription">
                                        <p>KOMPLEKSOWA OBSŁUGA PODMITÓW GOSPODARCZYCH</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/handshake.png" class="img-responsive" alt="handshake">
                                    </div>
                                    <div class="offerDescription">
                                        <p>PROWADZENIE NEGOCJACJI W IMIENIU KLIENTA</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 offerBox">
                                    <div class="offerIcon">
                                        <img src="images/tests.png" class="img-responsive" alt="test">
                                    </div>
                                    <div class="offerDescription">
                                        <p>SPORZĄDZANIE UMÓW I OPINI</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><img
                            src="images/blackArrowLeft.png" alt="arrow"></a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><img
                            src="images/blackArrowRight.png" alt="arrow"></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="specialization" class="specialization" style="background-image: url('images/specialization.jpg')">
    <div class="container-fluid nopadding minheight">
        <div class="imagePattern"></div>
        <div class="blackPattern2"></div>
        <div class="mainSectionCenter">
            <div class="mainDescription sectionDescription colorTextWhite">
                <h2>W CZYM SIĘ SPECIALIZUJEMY?</h2>
            </div>
            <div class="breakLineCenter"></div>
            <div class="aboutSection">
                <h3>Eu sanctus saperet mea, re vim partem tacimates</h3>
            </div>
            <a href="specialization.php" class="buttonMore buttonMore-1 buttonMore-2">czytaj więcej</a>
        </div>
    </div>
</section>

<section id="recommendation" class="recommendation">
    <div class="container-fluid nopadding">
        <div class="sectionBox">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>REKOMENDACJE</h2>
            </div>
            <div class="breakLineCenter"></div>

            <div class="recommendationCarousel">
                <div class="carousel slide" id="myCarousel2">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="recommendationBox">
                                <h2>“Lorem ipsum dolor sit amet, eu delenit mnesarchum nec, at mei dico doctus
                                    dolorum”</h2>
                                <h3>Jan Kowalski</h3>
                            </div>
                        </div>
                        <div class="item">
                            <div class="recommendationBox">
                                <h2>“Lorem ipsum dolor sit amet, eu delenit mnesarchum nec, at mei dico doctus
                                    dolorum”</h2>
                                <h3>Jan Kowalski</h3>
                            </div>
                        </div>
                        <div class="item">
                            <div class="recommendationBox">
                                <h2>“Lorem ipsum dolor sit amet, eu delenit mnesarchum nec, at mei dico doctus
                                    dolorum”</h2>
                                <h3>Jan Kowalski</h3>
                            </div>
                        </div>

                        <a class="left carousel-control" href="#myCarousel2" data-slide="prev"><img
                                src="images/blackArrowLeft.png" alt="arrow"></a>
                        <a class="right carousel-control" href="#myCarousel2" data-slide="next"><img
                                src="images/blackArrowRight.png" alt="arrow"></a>
                    </div>
                </div>
            </div>

        </div>
</section>

<section>
    <div class="container-fluid nopadding minheight">
        <div class="row">
            <div id="meetus" class="col-md-6 meetus minheight" style="background-image: url('images/meetus.jpg')">
                <div class="imagePattern"></div>
                <div class="blackPattern2"></div>
                <div class="mainSectionCenter mainSectionColumn">
                    <div class="mainDescription sectionDescription colorTextWhite">
                        <h2>POZNAJ NAS</h2>
                    </div>
                    <div class="breakLineCenter"></div>
                    <div class="aboutSection aboutSectionColumn">
                        <h3>Eu sanctus saperet mea, re vim partem tacimates</h3>
                    </div>
                    <a href="team.php" class="buttonMore buttonMore-1 buttonMore-2">czytaj więcej</a>
                </div>

            </div>
            <div id="aboutoffice" class="col-md-6 aboutoffice minheight" style="background-image: url('images/aboutoffice.jpg')">
                <div class="imagePattern"></div>
                <div class="blackPattern2"></div>
                <div class="mainSectionCenter mainSectionColumn">
                    <div class="mainDescription sectionDescription colorTextWhite">
                        <h2>KILKA SŁÓW O KANCELARII</h2>
                    </div>
                    <div class="breakLineCenter"></div>
                    <div class="aboutSection aboutSectionColumn">
                        <h3>Eu sanctus saperet mea, re vim partem tacimates</h3>
                    </div>
                    <a href="about.php" class="buttonMore buttonMore-1 buttonMore-2">czytaj więcej</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="map_page" class="map_page">
    <div id="map">
    </div>
</section>

<section id="contactForm" class="contactForm">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter sectionCenter">
            <div class="mainDescription sectionDescription colorTextGrey">
                <h2>NAPISZ DO NAS</h2>
            </div>
            <div class="breakLineCenter"></div>
        </div>
        <div class="form row">
            <form>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="name" class="form-control contact-form" id="inputName" placeholder="Wpisz Imię i Nazwisko">
                    <input type="text" name="company name" class="form-control contact-form" id="inputCompany" placeholder="Wpisz nazwę firmy">
                    <input type="tel" name="phone" class="form-control contact-form" id="inputPhone" placeholder="Wpisz numer telefonu">
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <input type="text" name="topic" class="form-control contact-form" id="inputSubject" placeholder="Wpisz temat">
                    <textarea class="form-control contact-area" id="inputMessage" placeholder="Wpisz treść wiadomości..."></textarea>
                </div>
            </form>
        </div>
        <a href="#" class="buttonMore buttonMore-1 buttonMore-2">wyślij</a>
        <div class="iconContact ">
            <ul>
                <li><a href="tel:12 349 06 84"><img src="images/iconphone.png" alt="phone">12 349 06 84</a></li>
                <li><a href="mailto:kancelaria@kancelaria-kpml.pl"><img src="images/iconmail.png" alt="mail">kancelaria@kancelaria-kpml.pl</a></li>
            </ul>
        </div>
    </div>
</section>

<section id="footerLogo" class="footerLogo">
    <div class="container-fluid nopadding minheight">
        <div class="mainSectionCenter">
            <div class="logoFooter">
                <a href="index.php"><img src="images/logomakan.svg" alt="logo"></a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright colorTextWhite"><p>JOANNA MAKAN 2016 | Wykonanie: <a class="colorTextRed" href="#">BeSite.pl</a></p></div>
</footer>

</body>

</html>